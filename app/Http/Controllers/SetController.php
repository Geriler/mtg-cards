<?php

namespace App\Http\Controllers;

use App\Set;
use Illuminate\Http\Request;

class SetController extends Controller
{
    public function index()
    {
        $sets = Set::all();

        return view('admin.set.index', [
            'sets' => $sets,
        ]);
    }

    public function create()
    {
        return view('admin.set.create');
    }

    public function store(Request $request)
    {
        Set::create($request->all());
        return redirect(route('admin.set.index'));
    }

    public function show(Set $set)
    {
        return redirect(route('admin.set.index'));
    }

    public function edit(Set $set)
    {
        $set = Set::find($set->id);
        return view('admin.set.edit', [
            'set' => $set,
        ]);
    }

    public function update(Request $request, Set $set)
    {
        $set = Set::find($set->id);
        $set->fill($request->all());
        $set->save();
        return redirect(route('admin.set.index'));
    }

    public function destroy(Set $set)
    {
        Set::find($set->id)->delete();
        return redirect(route('admin.set.index'));
    }
}
