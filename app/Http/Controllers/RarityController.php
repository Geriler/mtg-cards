<?php

namespace App\Http\Controllers;

use App\Rarity;
use Illuminate\Http\Request;

class RarityController extends Controller
{
    public function index()
    {
        $ratitys = Rarity::all();

        return view('admin.rarity.index', [
            'raritys' => $ratitys,
        ]);
    }

    public function create()
    {
        return view('admin.rarity.create');
    }

    public function store(Request $request)
    {
        Rarity::create($request->all());
        return redirect(route('admin.rarity.index'));
    }

    public function show(Rarity $rarity)
    {
        return redirect(route('admin.rarity.index'));
    }

    public function edit(Rarity $rarity)
    {
        $rarity = Rarity::find($rarity->id);
        return view('admin.rarity.edit', [
            'rarity' => $rarity
        ]);
    }

    public function update(Request $request, Rarity $rarity)
    {
        $rarity = Rarity::find($rarity->id);
        $rarity->fill($request->all());
        $rarity->save();
        return redirect(route('admin.rarity.index'));
    }

    public function destroy(Rarity $rarity)
    {
        Rarity::find($rarity->id)->delete();
        return redirect(route('admin.rarity.index'));
    }
}
