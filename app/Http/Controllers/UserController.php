<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('admin.user.index', [
            'users' => $users,
        ]);
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(Request $request)
    {
        $request->merge(['password' => Hash::make($request->password)]);
        if ($request->is_admin == 'on') {
            $request->merge(['is_admin' => 1]);
        } else {
            $request->merge(['is_admin' => 0]);
        }
        User::create($request->all());
        return redirect(route('admin.user.index'));
    }

    public function show(User $user)
    {
        return redirect(route('admin.user.index'));
    }

    public function edit(User $user)
    {
        $user = User::find($user->id);
        return view('admin.user.edit', [
            'user' => $user,
        ]);
    }

    public function update(Request $request, User $user)
    {
        $request->merge(['password' => Hash::make($request->password)]);
        if ($request->is_admin == 'on') {
            $request->merge(['is_admin' => 1]);
        } else {
            $request->merge(['is_admin' => 0]);
        }
        $user = User::find($user->id);
        $user->fill($request->all());
        $user->save();
        return redirect(route('admin.user.index'));
    }

    public function destroy(User $user)
    {
        User::find($user->id)->delete();
        return redirect(route('admin.user.index'));
    }
}
