<?php

namespace App\Http\Controllers;

use App\TypeCard;
use Illuminate\Http\Request;

class TypeCardController extends Controller
{
    public function index()
    {
        $typeCards = TypeCard::all();

        return view('admin.type_card.index', [
            'type_cards' => $typeCards,
        ]);
    }

    public function create()
    {
        return view('admin.type_card.create');
    }

    public function store(Request $request)
    {
        TypeCard::create($request->all());
        return redirect(route('admin.type_card.index'));
    }

    public function show(TypeCard $typeCard)
    {
        return redirect(route('admin.type_card.index'));
    }

    public function edit(TypeCard $typeCard)
    {
        $type = TypeCard::find($typeCard->id);
        return view('admin.type_card.edit', [
            'type_card' => $type,
        ]);
    }

    public function update(Request $request, TypeCard $typeCard)
    {
        $type = TypeCard::find($typeCard->id);
        $type->fill($request->all());
        $type->save();
        return redirect(route('admin.type_card.index'));
    }

    public function destroy(TypeCard $typeCard)
    {
        TypeCard::find($typeCard->id)->delete();
        return redirect(route('admin.type_card.index'));
    }
}
