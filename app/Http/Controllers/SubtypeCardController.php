<?php

namespace App\Http\Controllers;

use App\SubtypeCard;
use Illuminate\Http\Request;

class SubtypeCardController extends Controller
{
    public function index()
    {
        $subtypeCards = SubtypeCard::all();

        return view('admin.subtype_card.index', [
            'subtype_cards' => $subtypeCards,
        ]);
    }

    public function create()
    {
        return view('admin.subtype_card.create');
    }

    public function store(Request $request)
    {
        SubtypeCard::create($request->all());
        return redirect(route('admin.subtype_card.index'));
    }

    public function show(SubtypeCard $subtypeCard)
    {
        return redirect(route('admin.subtype_card.index'));
    }

    public function edit(SubtypeCard $subtypeCard)
    {
        $subtype = SubtypeCard::find($subtypeCard->id);
        return view('admin.subtype_card.edit', [
            'subtype_card' => $subtype,
        ]);
    }

    public function update(Request $request, SubtypeCard $subtypeCard)
    {
        $subtype = SubtypeCard::find($subtypeCard->id);
        $subtype->fill($request->all());
        $subtype->save();
        return redirect(route('admin.subtype_card.index'));
    }

    public function destroy(SubtypeCard $subtypeCard)
    {
        SubtypeCard::find($subtypeCard->id)->delete();
        return redirect(route('admin.subtype_card.index'));
    }
}
