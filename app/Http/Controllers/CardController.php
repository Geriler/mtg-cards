<?php

namespace App\Http\Controllers;

use App\Card;
use App\Color;
use App\Rarity;
use App\Set;
use App\SubtypeCard;
use App\TypeCard;
use Illuminate\Http\Request;

class CardController extends Controller
{
    public function index()
    {
        $cards = Card::all();

        return view('admin.card.index', [
            'cards' => $cards,
        ]);
    }

    public function create()
    {
        $typeCards = TypeCard::all();
        $subtypeCards = SubtypeCard::all();
        $colors = Color::all();
        $raritys = Rarity::all();
        $sets = Set::all()->sortByDesc('id');

        return view('admin.card.create', [
            'sets' => $sets,
            'type_cards' => $typeCards,
            'subtype_cards' => $subtypeCards,
            'colors' => $colors,
            'raritys' => $raritys,
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            preg_match('/.*(\..*)$/', $file->getClientOriginalName(), $matches);
            $name = $matches[0];
            $data['image'] = '/images/' . $name;
            $file->move(public_path() . '/images', $name);
        }
        Card::create($data);
        return redirect(route('admin.card.index'));
    }

    public function show(Card $card)
    {
        $card = Card::find($card->id);
        $rarity = Rarity::find($card->rarity_id);
        $set = Set::find($card->set_id);

        return view('admin.card.show', [
            'card' => $card,
            'rarity' => $rarity,
            'set' => $set,
        ]);
    }

    public function edit(Card $card)
    {
        $card = Card::find($card->id);
        $raritys = Rarity::all();
        $sets = Set::all();

        return view('admin.card.edit', [
            'card' => $card,
            'raritys' => $raritys,
            'sets' => $sets,
        ]);
    }

    public function update(Request $request, Card $card)
    {
        if (!empty($request->file)) {
            $request->file->storeAs('images', $request->file->getClientOriginalName(), 'public');
            $filename = 'images/' . $request->file->getClientOriginalName();
            $request->merge(['image' => $filename]);
        }
        $card = Card::find($card->id);
        $card->fill($request->all());
        $card->save();
        return redirect(route('admin.card.index'));
    }

    public function destroy(Card $card)
    {
        Card::find($card->id)->delete();
        return redirect(route('admin.card.index'));
    }
}
