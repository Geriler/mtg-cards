<?php

namespace App\Http\Controllers;

use App\Card;
use App\Set;

class HomepageController extends Controller
{
    public function index() {
        $cards = Card::all()->sortBy('name_ru');
        $sets = Set::all();

        return view('homepage', [
            'cards' => $cards,
            'sets' => $sets,
        ]);
    }
}
