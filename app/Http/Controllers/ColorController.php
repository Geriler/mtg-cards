<?php

namespace App\Http\Controllers;

use App\Color;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    public function index()
    {
        $colors = Color::all();

        return view('admin.color.index', [
            'colors' => $colors,
        ]);
    }

    public function create()
    {
        return view('admin.color.create');
    }

    public function store(Request $request)
    {
        Color::create($request->all());
        return redirect(route('admin.color.index'));
    }

    public function show(Color $color)
    {
        return redirect(route('admin.color.index'));
    }

    public function edit(Color $color)
    {
        $color = Color::find($color->id);
        return view('admin.color.edit', [
            'color' => $color
        ]);
    }

    public function update(Request $request, Color $color)
    {
        $color = Color::find($color->id);
        $color->fill($request->all());
        $color->save();
        return redirect(route('admin.color.index'));
    }

    public function destroy(Color $color)
    {
        Color::find($color->id)->delete();
        return redirect(route('admin.color.index'));
    }
}
