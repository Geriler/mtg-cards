<?php

namespace App\Http\Controllers;

use App\Card;
use App\Color;
use App\Rarity;
use App\Set;
use App\SubtypeCard;
use App\TypeCard;
use App\User;

class DashboardController extends Controller
{
    public function index() {
        $users = User::all();
        $sets = Set::all();
        $typeCards = TypeCard::all();
        $subtypeCards = SubtypeCard::all();
        $colors = Color::all();
        $raritys = Rarity::all();
        $cards = Card::all();

        return view('admin.dashboard', [
            'users' => $users,
            'sets' => $sets,
            'type_cards' => $typeCards,
            'subtype_cards' => $subtypeCards,
            'colors' => $colors,
            'raritys' => $raritys,
            'cards' => $cards,
        ]);
    }
}
