<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubtypeCard extends Model
{
    protected $fillable = ['name'];
}
