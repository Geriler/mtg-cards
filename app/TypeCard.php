<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeCard extends Model
{
    protected $fillable = ['name'];
}
