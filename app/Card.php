<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = [
        'name_ru', 'name_en', 'mana_cost', 'convert_mana_cost', 'image', 'set_id', 'count', 'foil', 'promo', 'in_deck', 'rarity_id'
    ];
}
