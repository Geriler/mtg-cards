<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomepageController@index')->name('homepage');

Auth::routes(['register' => false]);

Route::prefix('admin')->middleware('auth', 'is_admin')->name('admin.')->group(function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::resource('type_card', 'TypeCardController');
    Route::resource('subtype_card', 'SubtypeCardController');
    Route::resource('set', 'SetController');
    Route::resource('user', 'UserController');
    Route::resource('color', 'ColorController');
    Route::resource('rarity', 'RarityController');
    Route::resource('card', 'CardController');
});
