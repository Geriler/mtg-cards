<?php

use App\Set;
use Illuminate\Database\Seeder;

class SetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Set::create(['name' => 'Возвращение в Равнику']);
        Set::create(['name' => 'Незванные Гости']);
        Set::create(['name' => 'Лабиринт Дракона']);
        Set::create(['name' => 'M14']);
        Set::create(['name' => 'M17']);
        Set::create(['name' => 'Амонхет']);
        Set::create(['name' => 'Иксалан']);
        Set::create(['name' => 'Доминария']);
        Set::create(['name' => 'M19']);
        Set::create(['name' => 'Гильдии Равники']);
        Set::create(['name' => 'Выбор Равники']);
        Set::create(['name' => 'Война Искры']);
        Set::create(['name' => 'М20']);
        Set::create(['name' => 'Престол Элдраина']);
    }
}
