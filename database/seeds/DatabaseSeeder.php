<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SetsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TypeCardsTableSeeder::class);
        $this->call(ColorsTableSeeder::class);
        $this->call(RaritysTableSeeder::class);
    }
}
