<?php

use App\Rarity;
use Illuminate\Database\Seeder;

class RaritysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rarity::create(['name' => 'Обычная']);
        Rarity::create(['name' => 'Необычная']);
        Rarity::create(['name' => 'Редкая']);
        Rarity::create(['name' => 'Мифическая']);
        Rarity::create(['name' => 'Земля']);
        Rarity::create(['name' => 'Токен']);
    }
}
