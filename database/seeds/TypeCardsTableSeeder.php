<?php

use App\TypeCard;
use Illuminate\Database\Seeder;

class TypeCardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypeCard::create(['name' => 'Существо']);
        TypeCard::create(['name' => 'Мгновенное заклинание']);
        TypeCard::create(['name' => 'Волшебство']);
        TypeCard::create(['name' => 'Чары']);
        TypeCard::create(['name' => 'Артефакт']);
        TypeCard::create(['name' => 'Земля']);
        TypeCard::create(['name' => 'Базовая Земля']);
        TypeCard::create(['name' => 'Planeswalker']);
        TypeCard::create(['name' => 'Легендарный']);
        TypeCard::create(['name' => 'Токен']);
    }
}
