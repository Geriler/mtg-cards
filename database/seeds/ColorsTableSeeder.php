<?php

use App\Color;
use Illuminate\Database\Seeder;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Color::create(['name' => 'Белый']);
        Color::create(['name' => 'Синий']);
        Color::create(['name' => 'Красный']);
        Color::create(['name' => 'Зелёный']);
        Color::create(['name' => 'Чёрный']);
    }
}
