<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cards</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            li {
                list-style-type: none;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
        </style>
    </head>
    <body>
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    Hello, {{ Auth::user()->name }}

                    @if (Auth::user()->is_admin)
                        <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                    @endif

                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @endauth
            </div>
        @endif

        <div class="container-fluid">
            <div class="row mt-5">
                <?php $count = 0 ?>
                @foreach($cards as $card)
                    <div class="mt-2 mb-2 col-sm-4 col-md-3 col-lg-2">
                        <img src="{{ $card->image }}" alt="{{ $card->name_en }}" width="100%">
                        <table class="table">
                            <tr><td>{{ $card->name_ru }}</td></tr>
                            <tr><td>{{ $card->name_en }}</td></tr>
                            <tr><td>{{ $sets[$card->set_id - 1]->name }}</td></tr>
                            <tr>
                                <td>
                                    Всего: {{ $card->count }}
                                    <ul>
                                        <li>из них фойловые: {{ $card->foil }}</li>
                                        <li>из них промо: {{ $card->promo }}</li>
                                        <li>из них в колодах: {{ $card->in_deck }}</li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <?php $count += $card->count ?>
                @endforeach
            </div>
            <hr>
            <div>
                <b>Всего карт: {{ $count }}</b>
            </div>
        </div>
    </body>
</html>
