@extends('admin.layouts.app')

@section('content')
    <div class="btn-group">
        <a class="btn btn-primary mb-3" href="{{ route('admin.subtype_card.create') }}"><i class="fa fa-plus"></i> Добавить</a>
    </div>
    <div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($subtype_cards as $subtype)
                <tr>
                    <th scope="col">{{ $subtype->id }}</th>
                    <td>{{ $subtype->name }}</td>
                    <td>
                        <div class="btn-group">
                            <a class="btn btn-block" href="{{ route('admin.subtype_card.edit', $subtype->id) }}"><i class="fa fa-edit"></i></a>
                            <form action="{{ route('admin.subtype_card.destroy', $subtype->id) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
