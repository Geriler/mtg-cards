@extends('admin.layouts.app')

@section('content')
    <div class="btn-group">
        <a class="btn btn-primary mb-3" href="{{ route('admin.card.create') }}"><i class="fa fa-plus"></i> Добавить</a>
    </div>
    <div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col"></th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cards as $card)
                <tr>
                    <th scope="col">{{ $card->id }}</th>
                    <td>{{ $card->name_ru }}</td>
                    <td>{{ $card->name_en }}</td>
                    <td>
                        <div class="btn-group">
                            <a class="btn text-primary" href="{{ route('admin.card.show', $card->id) }}"><i class="fa fa-eye"></i></a>
                            <a class="btn text-secondary" href="{{ route('admin.card.edit', $card->id) }}"><i class="fa fa-edit"></i></a>
                            <form action="{{ route('admin.card.destroy', $card->id) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <button class="btn text-danger" type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
