@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.card.store') }}" method="post" enctype="multipart/form-data">
        <div class="form-group">
            {{ csrf_field() }}
            <label>
                Название на русском<br>
                <input type="text" name="name_ru"><br>
            </label>
            <label>
                Название на английском<br>
                <input type="text" name="name_en"><br>
            </label><br>
            <label>
                Mana cost<br>
                {W} - Белый<br>
                {U} - Синий<br>
                {B} - Чёрный<br>
                {R} - Красный<br>
                {G} - Зелёный<br>
                {X} - Любой цвет<br>
                <input type="text" name="mana_cost"><br>
            </label><br>
            <label>
                Конвертированный mana cost<br>
                <input type="number" name="convert_mana_cost" min="0" value="0"><br>
            </label><br>
            <label>
                Изображение<br>
                <input type="file" name="file" accept="image/*"><br>
            </label><br>
            <label>
                Set<br>
                <select name="set_id">
                    @foreach($sets as $set)
                        <option value="{{ $set->id }}">{{ $set->name }}</option>
                    @endforeach
                </select>
            </label><br>
            <label>
                Rarity<br>
                <select name="rarity_id">
                    @foreach($raritys as $rarity)
                        <option value="{{ $rarity->id }}">{{ $rarity->name }}</option>
                    @endforeach
                </select>
            </label><br>
            <label>
                Количество<br>
                <input type="number" name="count" min="0" value="0"><br>
            </label><br>
            <label>
                из них фойловые<br>
                <input type="number" name="foil" min="0" value="0"><br>
            </label>
            <label>
                из них промо<br>
                <input type="number" name="promo" min="0" value="0"><br>
            </label>
            <label>
                из них в колодах<br>
                <input type="number" name="in_deck" min="0" value="0"><br>
            </label><br>
            <button class="btn btn-primary"><i class="fa fa-save"></i> Сохранить</button>
        </div>
    </form>
@endsection
