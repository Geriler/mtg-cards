@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <a class="btn btn-primary mb-3" href="{{ route('admin.card.edit', $card->id) }}"><i class="fa fa-edit"></i> Изменить</a>
        </div>
        <div class="col-sm-12 col-md-3">
            <img src="{{ asset($card->image) }}" alt="{{ $card->name_en }}" width="100%">
        </div>
        <div class="col-sm-12 col-md-9">
            <table class="table table-striped">
                <tr>
                    <td>Название</td>
                    <td>{{ $card->name_ru }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{ $card->name_en }}</td>
                </tr>
                <tr>
                    <td>Мана-стоимость</td>
                    <td>{{ $card->mana_cost }}</td>
                </tr>
                <tr>
                    <td>Конвертированная мана-стоимость</td>
                    <td>{{ $card->convert_mana_cost }}</td>
                </tr>
                <tr>
                    <td>Редкость</td>
                    <td>{{ $rarity->name }}</td>
                </tr>
                <tr>
                    <td>Set</td>
                    <td>{{ $set->name }}</td>
                </tr>
                <tr>
                    <td>Количество</td>
                    <td>
                        Всего: {{ $card->count }}
                        <ul>
                            <li>из них фойловые: {{ $card->foil }}</li>
                            <li>из них промо: {{ $card->promo }}</li>
                            <li>из них в колодах: {{ $card->in_deck }}</li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
@endsection
