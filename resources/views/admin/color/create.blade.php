@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.color.store') }}" method="post">
        <div class="form-group">
            {{ csrf_field() }}
            <label>
                Название<br>
                <input type="text" name="name"><br>
            </label><br>
            <button class="btn btn-primary"><i class="fa fa-save"></i> Сохранить</button>
        </div>
    </form>
@endsection
