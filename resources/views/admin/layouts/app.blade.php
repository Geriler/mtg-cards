<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Dashboard</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://kit.fontawesome.com/c79fe8c93a.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ route('homepage') }}">Homepage</a>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-dark sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item"><a class="nav-link" href="{{ route('admin.dashboard') }}"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('admin.user.index') }}"><i class="fa fa-user"></i> Users</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('admin.set.index') }}"><i class="fa fa-list-ul"></i> Sets</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('admin.card.index') }}"><i class="fa fa-layer-group"></i> Cards</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('admin.type_card.index') }}"><i class="fa fa-file-alt"></i> Type cards</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('admin.subtype_card.index') }}"><i class="fa fa-file-alt"></i> Subtype cards</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('admin.color.index') }}"><i class="fa fa-file-alt"></i> Colors</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('admin.rarity.index') }}"><i class="fa fa-file-alt"></i> Rarity</a></li>
                    </ul>
                </div>
            </nav>
            <main class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4" role="main">
                @yield('content')
            </main>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>
