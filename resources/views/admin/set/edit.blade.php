@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.set.update', $set->id) }}" method="post">
        <div class="form-group">
            {{ method_field('put') }}
            {{ csrf_field() }}
            <label>
                Название<br>
                <input type="text" name="name" value="{{ $set->name }}"><br>
            </label><br>
            <button class="btn btn-primary"><i class="fa fa-save"></i> Сохранить</button>
        </div>
    </form>
@endsection
