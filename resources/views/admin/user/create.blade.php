@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.user.store') }}" method="post">
        <div class="form-group">
            {{ csrf_field() }}
            <label>
                Имя<br>
                <input type="text" name="name"><br>
            </label><br>
            <label>
                Email<br>
                <input type="email" name="email"><br>
            </label><br>
            <label>
                Пароль<br>
                <input type="password" name="password" required><br>
            </label><br>
            <label>
                <input type="checkbox" name="is_admin"> Администратор
            </label><br>
            <button class="btn btn-primary"><i class="fa fa-save"></i> Сохранить</button>
        </div>
    </form>
@endsection
