@extends('admin.layouts.app')

@section('content')
    <div class="btn-group">
        <a class="btn btn-primary mb-3" href="{{ route('admin.user.create') }}"><i class="fa fa-plus"></i> Добавить</a>
    </div>
    <div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Администратор</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="col">{{ $user->id }}</th>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @if($user->is_admin)
                            <i class="fa fa-check text-success"></i>
                        @else
                            <i class="fa fa-times text-danger"></i>
                        @endif
                    </td>
                    <td>
                        <div class="btn-group">
                            <a class="btn btn-block" href="{{ route('admin.user.edit', $user->id) }}"><i class="fa fa-edit"></i></a>
                            <form action="{{ route('admin.user.destroy', $user->id) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
