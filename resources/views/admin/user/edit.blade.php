@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.user.update', $user->id) }}" method="post">
        <div class="form-group">
            {{ method_field('put') }}
            {{ csrf_field() }}
            <label>
                Имя<br>
                <input type="text" name="name" value="{{ $user->name }}"><br>
            </label><br>
            <label>
                Email<br>
                <input type="email" name="email" value="{{ $user->email }}"><br>
            </label><br>
            <label>
                Пароль<br>
                <input type="password" name="password" required><br>
            </label><br>
            <label>
                <input type="checkbox" name="is_admin"> Администратор
            </label><br>
            <button class="btn btn-primary"><i class="fa fa-save"></i> Сохранить</button>
        </div>
    </form>
@endsection
